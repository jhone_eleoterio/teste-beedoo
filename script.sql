use beedoo_dev_test;

CREATE TABLE teams (
   id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
   name varchar(50) NOT NULL DEFAULT ''
);

INSERT INTO teams (name) value ('team 1');
INSERT INTO teams (name) value ('team 2');
INSERT INTO teams (name) value ('team 3');

CREATE TABLE users (
   id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
   fullname varchar(150) NOT NULL DEFAULT '',
   username varchar(100) NOT NULL DEFAULT '',
   password varchar(100) NOT NULL DEFAULT '',
   created_at datetime NOT NULL DEFAULT NOW(),
   team_id integer not null default -1,
   CONSTRAINT fk_teams_users FOREIGN KEY (team_id) REFERENCES teams(id)
);

INSERT INTO users (fullname, username, password, team_id) values ('Roberto Caleb André Aparício', 'beebot01', '1234', 1);
INSERT INTO users (fullname, username, password, team_id) values ('Alexandre Antonio Almeida', 'beebot02', '1234', 2);
INSERT INTO users (fullname, username, password, team_id) values ('Renato Guilherme Figueiredo', 'beebot03', '1234', 3);

CREATE TABLE groups (
   id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
   name varchar(50) NOT NULL DEFAULT '',
   team_id integer NOT NULL DEFAULT -1,
   CONSTRAINT fk_teams_groups FOREIGN KEY (team_id) REFERENCES teams(id)
);

INSERT INTO groups (name, team_id) values ('group01', 1);
INSERT INTO groups (name, team_id) values ('group02', 1);
INSERT INTO groups (name, team_id) values ('group03', 1);
INSERT INTO groups (name, team_id) values ('group04', 2);
INSERT INTO groups (name, team_id) values ('group05', 3);

CREATE TABLE users_has_groups (
   id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
   group_id integer NOT NULL DEFAULT -1,
   user_id integer NOT NULL DEFAULT -1   
);

INSERT INTO users_has_groups (group_id, user_id) values (1, 1);
INSERT INTO users_has_groups (group_id, user_id) values (2, 2);
INSERT INTO users_has_groups (group_id, user_id) values (3, 3);
INSERT INTO users_has_groups (group_id, user_id) values (4, 1);
INSERT INTO users_has_groups (group_id, user_id) values (5, 1);

CREATE TABLE posts (
   id integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
   title varchar(40) NOT NULL DEFAULT '',
   content varchar(8000) NOT NULL DEFAULT '',
   created_at datetime NOT NULL DEFAULT NOW(),
   user_id integer NOT NULL DEFAULT -1,
   CONSTRAINT fk_posts_users FOREIGN KEY (user_id) REFERENCES users(id)
);

INSERT INTO posts (title, content, user_id) values ('Lorem 1', 
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tempus tellus at sagittis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer ut lobortis mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum at blandit nisl. Ut in tempor tellus. Morbi ut nibh ut risus dignissim varius ac vel enim. Vestibulum laoreet congue lacinia. Duis interdum varius odio id vehicula. In justo enim, posuere at auctor et, pellentesque sed felis. Mauris ut nisl hendrerit nisl dignissim dapibus at ut diam. Fusce efficitur, nisi sit amet luctus placerat, felis erat semper augue, sed aliquet ipsum est hendrerit odio. Duis quis risus tempor, varius leo vel, dictum enim.', 1);

INSERT INTO posts (title, content, user_id) values ('Lorem Ipsum 2', 
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tempus tellus at sagittis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer ut lobortis mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum at blandit nisl. Ut in tempor tellus. Morbi ut nibh ut risus dignissim varius ac vel enim. Vestibulum laoreet congue lacinia. Duis interdum varius odio id vehicula. In justo enim, posuere at auctor et, pellentesque sed felis. Mauris ut nisl hendrerit nisl dignissim dapibus at ut diam. Fusce efficitur, nisi sit amet luctus placerat, felis erat semper augue, sed aliquet ipsum est hendrerit odio. Duis quis risus tempor, varius leo vel, dictum enim.', 1);

INSERT INTO posts (title, content, user_id) values ('Lorem Ipsum 3', 
'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tempus tellus at sagittis maximus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Integer ut lobortis mi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum at blandit nisl. Ut in tempor tellus. Morbi ut nibh ut risus dignissim varius ac vel enim. Vestibulum laoreet congue lacinia. Duis interdum varius odio id vehicula. In justo enim, posuere at auctor et, pellentesque sed felis. Mauris ut nisl hendrerit nisl dignissim dapibus at ut diam. Fusce efficitur, nisi sit amet luctus placerat, felis erat semper augue, sed aliquet ipsum est hendrerit odio. Duis quis risus tempor, varius leo vel, dictum enim.', 1);

