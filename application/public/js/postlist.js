$(document).ready(function() {
    $('#table').DataTable( {
        "processing": true,
        "serverSide": false,
        "ajax": {
            "url": "/posts/get",
            "type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "title" },
            { "data": "treated_datetime" },
            { "data": "fullname" }
        ]
    } );
} );
