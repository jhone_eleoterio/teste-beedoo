$(document).ready(function() {
    $('#table').DataTable( {
        "processing": false,
        "serverSide": true,
        "ajax": {
            "url": "/groups/get",
            "type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "team" }
        ]
    } );
} );