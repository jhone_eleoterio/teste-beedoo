$(document).ready(function() {
    $('#table').DataTable( {
        "processing": false,
        "serverSide": true,
        "ajax": {
            "url": "/users/get",
            "type": "POST"
        },
        "columns": [
            { "data": "id" },
            { "data": "username" },
            { "data": "fullname" },
            { "data": "treated_datetime" }
        ]
    } );
} );