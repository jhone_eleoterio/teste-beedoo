<?php
class Default_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function team($username, $password) 
    {   
        $sql = "SELECT 
                   u.id, 
                   u.username,
                   t.name,
                   u.team_id
                FROM users u 
                INNER JOIN teams t on t.id = u.team_id
                WHERE username = ? AND password = ?";
        $query = $this->db->query($sql, array($username, $password));
        return $query->row();
        
    }
    
}