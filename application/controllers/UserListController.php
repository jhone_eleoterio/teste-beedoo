<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserListController extends CI_Controller {

    /**
     * @var Users_model
     */
    public $Model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Users_model','model');
    }

    public function index()
    {
        $this->load->view('userlist', $this->viewData);
    }

    public function get()
    {
        $team = $this->session->userdata('team');
        $this->load->library('Datatables');
        $this->datatables
            ->select('id, 
                      username,    
                      fullname, 
                      DATE_FORMAT(created_at, \'%d/%m/%Y %H:%i\') as treated_datetime'
                , false)
            ->from('users')
            ->where('team_id', $team->team_id);

	    echo $this->datatables->generate();
    }
}