<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

    /**
     * @var Posts_model
     */
    public $Model;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Posts_model');
        $this->load->helper('url_helper');
    }

    public function index()
    {
        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('pass', 'Password', 'required');
        
        if ($this->form_validation->run() == FALSE)
        {
                $this->load->view('login');
        }
    }

    public function login()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('pass', 'Password', 'required');
        
        if ($this->form_validation->run() === FALSE)
        {
           $this->load->view('login');    
        }
        else
        {
            $this->load->model('Default_model', 'defaultModel');
            
            $username = $this->input->post('username');
            $senha = $this->input->post('pass');       
            $this->session->set_userdata('team', $this->defaultModel->team($username, $senha));
            $this->load->helper('url');
            redirect('http://'.$_SERVER['HTTP_HOST'].'/home', 'refresh');     
        }
            
    }
}