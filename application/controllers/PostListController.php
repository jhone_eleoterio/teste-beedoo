<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PostListController extends CI_Controller {

    /**
     * @var Posts_model
     */
    public $Model;
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Posts_model', 'model');
        $this->user = $this->session->userdata('team');
    }

    public function index()
    {
        $this->load->helper('url');
        $this->load->view('postlist', $this->viewData);
    }

    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->view('postcreate');   
    }

    public function save()
    {
       $this->load->helper('form');
       $this->load->library('form_validation');
       
       $this->form_validation->set_rules('title', 'Title', 'required');
       $this->form_validation->set_rules('content', 'Content', 'required');

       if ($this->form_validation->run() === FALSE)
       {  
          $this->load->view('postcreate');
       }
       else
       {
          $title = $this->input->post('title');
          $content = $this->input->post('content');
          $this->model->insert_post($title, $content, $this->user->id);
          $this->load->helper('url');
          redirect('http://'.$_SERVER['HTTP_HOST'].'/posts/', 'refresh');
       }
    }

    public function get()
    {   
       $this->load->library('Datatables');
       $this->datatables
            ->select(' distinct
                       SQL_CALC_FOUND_ROWS P.id, 
                       title, 
                       DATE_FORMAT(P.created_at, \'%d/%m/%Y %H:%i\') as treated_datetime,
                       U.fullname '
                , false)
            ->join('users AS U', 'P.user_id = U.id', 'inner')
            ->join('users_has_groups AS UG', 'UG.user_id = U.id', 'left')
            ->join('groups G', 'G.id = UG.group_id', 'inner')
            ->join('teams AS T', 'G.team_id = T.id', 'inner')
            ->from('posts AS P')
            ->where('P.user_id', $this->user->id);

	    echo $this->datatables->generate();
    }
}