<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GroupController extends CI_Controller {

    public $user;

    public function __construct()
    {
       parent::__construct();
       $this->load->model('Groups_model', 'model');
       $this->user = $this->session->userdata('team');
    }

    public function index() {
        $this->load->view('grouplist', $this->viewData);        
    }

    public function get() {
        $this->load->library('Datatables');
        $this->datatables
              ->select('G.id,
                       G.name,
                       T.name as team',
                       false)
            ->join('users_has_groups AS UG', 'UG.group_id = G.id', 'inner')
            ->join('teams AS T', 'T.id = G.team_id', 'inner')
            ->from('groups G')->where('G.team_id', $this->user->team_id);
        echo $this->datatables->generate();    
    }
}
