<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
 <?php require_once 'partials/header.php'?>
<body>

<div class="container">
    <h4 class="col-md-12 text-right" style="margin-top: 50px;">New Post</h4>

    <div class="row" style="margin: 20px 0">
        <?php require_once 'partials/menu.php' ?>
    </div>

    <?php 
       if(validation_errors() != null):
    ?>
    
    <div class="alert alert-dark alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>

        </button>
        <strong>Ops!</strong><?php echo validation_errors(); ?>  
    </div>

    <?php   
       endif;
    ?>

    <form method="post" action="save">
    <div class="form-group">
      <label for="">Title</label>
      <input type="text" class="form-control" name="title" id="" aria-describedby="helpId" placeholder="" >
    </div>
    
    <div class="form-group">
       <label for="Content">Content</label>
       <textarea class="form-control" name="content" id="" rows="5" ></textarea>
    </div>

    <div class="form-group">
       <input name="" id="" class="btn btn-success" type="submit" value="Add">
    </div>

    </form>
</div>

</body>
</html>